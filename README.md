# O1 Sensitivity Curves 


Files with .txt extension are two-column frequency, ASD ASCII files.

Files with .ecsv extension have ECSV comments lines in the file headers.
 
## L1 curve: 
`2015_10_24_15_10_43_L1_O1_strain.*`

From https://dcc.ligo.org/LIGO-G1600151:

"These are calibrated strain and displacement spectra from a representative lock
stretch for the beginning of O1, taken on Oct 24 2015, when the inspiral range
was 73 Mpc."

***

## H1 curve:
`2015_10_24_15_09_43_H1_O1_strain.*`

From https://dcc.ligo.org/LIGO-G1600150:

"These are calibrated strain and displacement spectra from a representative lock
stretch for the beginning of O1, taken on Oct 24 2015, when the inspiral range
was 80 Mpc."

***
